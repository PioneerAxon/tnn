#include <binary_tree.h>

///
/// \brief Node structure for BinaryTree
///
typedef struct _BinaryTreeNode
{
	BinaryTreeNode* left;		///< Left child
	BinaryTreeNode* right;		///< Right child
	BinaryTreeNode* parent;		///< Parent node
	void* data;			///< Data
} BinaryTreeNode;

static BinaryTreeNode* _binary_tree_create_node (BinaryTreeNode* parent, void* data)
{
	BinaryTreeNode* binary_tree_node;
	binary_tree_node = (BinaryTreeNode*) malloc (sizeof (BinaryTreeNode));
	assert (binary_tree_node);
	binary_tree_node->data = data;
	binary_tree_node->left = NULL;
	binary_tree_node->right = NULL;
	binary_tree_node->parent = parent;
	return binary_tree_node;
}

static void _binary_tree_destroy_node (BinaryTreeNode* node)
{
	free (node);
}

static void _binary_tree_insert_node_by_data (BinaryTreeNode* tree, void* data)
{
	if (tree->data > data)
	{
		if (tree->left)
		{
			_binary_tree_insert_node_by_data (tree->left, data);
			return;
		}
		else
		{
			tree->left = _binary_tree_create_node (tree, data);
			return;
		}
	}
	else if (tree->data < data)
	{
		if (tree->right)
		{
			_binary_tree_insert_node_by_data (tree->right, data);
			return;
		}
		else
		{
			tree->right = _binary_tree_create_node (tree, data);
			return;
		}
	}
}

static void _binary_tree_remove_node (BinaryTreeNode* node)
{
	BinaryTreeNode* tmp;
	if (node->parent == NULL)
	{
		_binary_tree_destroy_node (node);
	}
	else if (node == node->parent->right)
	{
		if (node->left == NULL && node->right == NULL)
		{
			node->parent->right = NULL;
		}
		else if (node->left != NULL && node->right == NULL)
		{
			node->parent->right = node->left;
			node->left->parent = node->parent;
		}
		else if (node->left == NULL && node->right != NULL)
		{
			node->parent->right = node->right;
			node->right->parent = node->parent;
		}
		else
		{
			node->parent->right = node->right;
			node->right->parent = node->parent;
			tmp = node->right;
			while (tmp->left != NULL) tmp = tmp->left;
			tmp->left = node->left;
			node->left->parent = tmp;
		}
		_binary_tree_destroy_node (node);
	}
	else if (node == node->parent->left)
	{
		if (node->left == NULL && node->right == NULL)
		{
			node->parent->left = NULL;
		}
		else if (node->left != NULL && node->right == NULL)
		{
			node->parent->left = node->left;
			node->left->parent = node->parent;
		}
		else if (node->left == NULL && node->right != NULL)
		{
			node->parent->left = node->right;
			node->right->parent = node->parent;
		}
		else
		{
			node->parent->left = node->right;
			node->right->parent = node->parent;
			tmp = node->right;
			while (tmp->left != NULL) tmp = tmp->left;
			tmp->left = node->left;
			node->left->parent = tmp;
		}
		_binary_tree_destroy_node (node);
	}
}

static void _binary_tree_remove_node_by_data (BinaryTreeNode* node, void* data)
{
	if (node->data == data)
	{
		_binary_tree_remove_node (node);
	}
	else
	{
		if (node->left)
			_binary_tree_remove_node_by_data (node->left, data);
		if (node->right)
			_binary_tree_remove_node_by_data (node->right, data);
	}
}

static void _binary_tree_process_destroy (BinaryTreeNode* node, void (*function) (void* data))
{
	if (node->left)
	{
		_binary_tree_process_destroy (node->left, function);
	}
	if (node->right)
	{
		_binary_tree_process_destroy (node->right, function);
	}
	if (function)
		(*function) (node->data);
	_binary_tree_remove_node (node);
}

BinaryTree* binary_tree_new ()
{
	BinaryTree* new_binary_tree;
	new_binary_tree = (BinaryTree*) malloc (sizeof (BinaryTree));
	assert (new_binary_tree);
	new_binary_tree->root = NULL;
	return new_binary_tree;
}

void binary_tree_destroy (BinaryTree* binary_tree)
{
	if (binary_tree->root)
	{
		_binary_tree_process_destroy (binary_tree->root, NULL);
	}
	free (binary_tree);
}

void binary_tree_insert_data (BinaryTree* binary_tree, void* data)
{
	if (binary_tree->root == NULL)
	{
		binary_tree->root = _binary_tree_create_node (NULL, data);
		return;
	}
	_binary_tree_insert_node_by_data (binary_tree->root, data);
}

void binary_tree_remove_data (BinaryTree* binary_tree, void* data)
{
	if (binary_tree->root)
	{
		if (binary_tree->root->data == data)
		{
			_binary_tree_destroy_node (binary_tree->root);
			binary_tree->root = NULL;
		}
		else
		{
			_binary_tree_remove_node_by_data (binary_tree->root, data);
		}
	}
}

void binary_tree_iterate_destroy (BinaryTree* binary_tree, void (*function) (void* data))
{
	if (binary_tree->root)
	{
		_binary_tree_process_destroy (binary_tree->root, function);
		binary_tree->root = NULL;
	}
}
