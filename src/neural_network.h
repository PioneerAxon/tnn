#ifndef __NEURAL_NETWORK_H__
#define __NEURAL_NETWORK_H__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

#include <neuron.h>
#include <dendrite.h>
#include <binary_tree.h>

typedef struct _NeuralNetworkPrivate NeuralNetworkPrivate;
typedef struct _Neuron Neuron;
typedef struct _BinaryTree BinaryTree;
typedef struct _Dendrite Dendrite;

///
/// \brief NeuralNetwork structure
///
typedef struct _NeuralNetwork
{
	int neuron_count;		///< Count of Neuron instances
	Neuron** neurons;		///< Neuron instance array
	NeuralNetworkPrivate* priv;	///< Private components of NeuralNetwork
	int input_dendrite_count;	///< Count of input Dendrite instances
	Dendrite** input_dendrites;	///< Input Dendrite instances
	int output_dendrite_count;	///< Count of output Dendrite instances
	Dendrite** output_dendrites;	///< Output Dendrite instances
} NeuralNetwork;

///
/// \brief NeuralNetwork constructor
///
/// Create a NeuralNetwork instance.
///
/// \return New NeuralNetwork instance
///
NeuralNetwork* neural_network_new ();

///
/// \brief NeuralNetwork destructor
///
/// Destroy a NeuralNetwork instance.
///
/// \param[in,out] neural_network NeuralNetwork instance
///
void neural_network_destroy (NeuralNetwork* neural_network);

///
/// \brief Insert Neuron in NeuralNetwork
///
/// Insert a Neuron instance into a NeuralNetwork instance.
///
/// \param[in] neural_network NeuralNetwork instance
/// \param[in] neuron Neuron instance
///
void neural_network_insert_neuron (NeuralNetwork* neural_network, Neuron* neuron);

///
/// \brief Remove Neuron from NeuralNetwork
///
/// Remove a Neuron instance from NeuralNetwork instance.
///
/// \param[in] neural_network NeuralNetwork instance
/// \param[in] neuron Neuron instance
///
void neural_network_remove_neuron (NeuralNetwork* neural_network, Neuron* neuron);

///
/// \brief Register input Neuron
///
/// Register input Neuron instance for NeuralNetwork instance.
///
/// \param[in] neural_network NeuralNetwork instance
/// \param[in] neuron Neuron instance
///
void neural_network_register_input_neuron (NeuralNetwork* neural_network, Neuron* neuron);

///
/// \brief Reggister output Neuron
///
/// Register output Neuron instance for NeuralNetwork instance.
///
/// \param[in] neural_network NeuralNetwork instance
/// \param[in] neuron Neuron instance
///
void neural_network_register_output_neuron (NeuralNetwork* neural_network, Neuron* neuron, float weight);

///
/// \brief Set input values
///
/// Set input values for input Neurons in NeuralNetwork instance.
///
/// \param[in] neural_network NeuralNetwork instance
/// \param[in] inputs Input values
///
void neural_network_set_inputs (NeuralNetwork* neural_network, float* inputs);

///
/// \brief Register stimulated Neurons
///
/// Register stimulated Neuron instances for upcoming calculation iteratoin.
///
/// \param[in] neural_network NeuralNetwork instance
/// \param[in] neuron Neuron instance
///
void neural_network_register_neuron_stimuli (NeuralNetwork* neural_network, Neuron* neuron);

///
/// \brief Run a calculation iteration
///
/// Run a calculation iteration over NeuralNetwork.
///
/// \param[in] neural_network NeuralNetwork instance
///
void neural_network_iterate (NeuralNetwork* neural_network);

///
/// \brief Get outputs from NeuralNetwork
///
/// Get outputs from output Neurons of NeuralNetwork instance.
///
/// \param[in] neural_network NeuralNetwork instance
/// \param[out] output Output values
///
void neural_network_get_outputs (NeuralNetwork* neural_network, float* output);

#endif
