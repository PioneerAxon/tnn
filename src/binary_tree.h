#ifndef __BINARY_TREE_H__
#define __BINARY_TREE_H__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

typedef struct _BinaryTreeNode BinaryTreeNode;

///
/// \brief BinaryTree structure
///
typedef struct _BinaryTree
{
	BinaryTreeNode* root;		///< BinaryTree root node
} BinaryTree;

///
/// \brief BinaryTree constructor
///
/// Create a new BinaryTree instance.
///
/// \return BinaryTree instance
///
BinaryTree* binary_tree_new ();

///
/// \brief BinaryTree destructor
///
/// Destroy BinaryTree instance.
///
/// \param[in] binary_tree BinaryTree instance
///
void binary_tree_destroy (BinaryTree* binary_tree);

///
/// \brief Insert into BinaryTree
///
/// Insert data into BinaryTree instance.
///
/// \param[in] binary_tree BinaryTree instance
/// \param[in] data Data
///
void binary_tree_insert_data (BinaryTree* binary_tree, void* data);

///
/// \brief Remove data from BinaryTree
///
/// Remove data from BinaryTree instance.
///
/// \param[in] binary_tree BinaryTree instance
/// \param[in] data Data
///
void binary_tree_remove_data (BinaryTree* binary_tree, void* data);

///
/// \brief Iterate and destroy BinaryTree
///
/// Iterate over and destroy data in BinaryTree by calling a callback function for each node.
///
/// \param[in] binary_tree BinaryTree instance
/// \param[in] function Iteration function.
///
void binary_tree_iterate_destroy (BinaryTree* binary_tree, void (*function) (void* data));

#endif
