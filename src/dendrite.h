#ifndef __DENDRITE_H__
#define __DENDRITE_H__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

#include <dendritic_tree.h>
#include <synapse.h>

typedef struct _DendritePrivate DendritePrivate;
typedef struct _DendriticTree DendriticTree;
typedef struct _Synapse Synapse;

///
/// \brief Dendrite structure
///
typedef struct _Dendrite
{
	float charge;			///< Chage on Dendrite branch
	DendritePrivate* priv;		///< Private component of Dendrite structure
} Dendrite;

///
/// \brief Dendrite constructor
///
/// Create a Dendrite instance for DendriticTree instance.
///
/// \param[in] parent DendriticTree instance
///
/// \return New Dendrite instance
///
Dendrite* dendrite_new (DendriticTree* parent);

///
/// \brief Dendrite destructor
///
/// Destroy a Dendrite instance.
///
/// \param[in,out] dendrite Dendrite instance
///
void dendrite_destroy (Dendrite* dendrite);

///
/// \brief Get parent DendriticTree
///
/// Get parent instance of DendriticTree.
///
/// \param[in] dendrite Dendrite instance
///
/// \return DendriticTree instance
///
DendriticTree* dendrite_get_dendritic_tree (Dendrite* dendrite);

///
/// \brief Add Synapse to Dendrite
///
/// Add Synapse instance to Dendrite instance.
///
/// \param[in] dendrite Dendrite instance
/// \param[in] synapse Synapse instance
///
void dendrite_set_synapse (Dendrite* dendrite, Synapse* synapse);

///
/// \brief Remove Synapse from Dendrite
///
/// Remove Synapse instance from Dendrite instance.
///
/// \param[in] dendrite Dendrite instance
///
void dendrite_remove_synapse (Dendrite* dendrite);

///
/// \brief Capture charge from Synapse
///
/// Capture charge from Synapse into Dendrite instance.
///
/// \param[in] dendrite Dendrite instance
/// \param[in] charge Chanrge to be captured
///
void dendrite_capture (Dendrite* dendrite, float charge);

#endif
