#include <neuron.h>


///
/// \brief Private components of Neuron structure
///
typedef struct _NeuronPrivate
{
	NeuralNetwork* neural_network;		///< Parent NeuralNetwork instance
	float threshold;			///< Threshold charge for firing event
} NeuronPrivate;

Neuron* neuron_new (NeuralNetwork* neural_network)
{
	Neuron* new_neuron;
	new_neuron = (Neuron*) malloc (sizeof (Neuron));
	assert (new_neuron);
	new_neuron->dendritic_tree = dendritic_tree_new (new_neuron);
	new_neuron->axon = axon_new (new_neuron);
	new_neuron->priv = (NeuronPrivate*) malloc (sizeof (NeuronPrivate));
	assert (new_neuron->priv);
	new_neuron->priv->neural_network = neural_network;
	new_neuron->charge = 0;
	new_neuron->priv->threshold = 1.0f;
	new_neuron->registered = 0;
	return new_neuron;
}

void neuron_destroy (Neuron* neuron)
{
	if (!neuron)
		return;
	dendritic_tree_destroy (neuron->dendritic_tree);
	axon_destroy (neuron->axon);
	if (neuron->priv)
		free (neuron->priv);
	free (neuron);
}

void neuron_set_threshold (Neuron* neuron, float threshold)
{
	neuron->priv->threshold = threshold;
}

float neuron_get_threshold (Neuron* neuron)
{
	return neuron->priv->threshold;
}

void neuron_stimulate (Neuron* neuron)
{
	if (neuron->registered == 0)
	{
		neural_network_register_neuron_stimuli (neuron->priv->neural_network, neuron);
		neuron->charge += dendritic_tree_process_charge (neuron->dendritic_tree);
		neuron->registered = 1;
	}
}

void neuron_process_stimuli (Neuron* neuron)
{
	neuron->registered = 0;
	if (neuron->charge >= neuron->priv->threshold)
	{
		while (neuron->charge > neuron->priv->threshold)
			neuron->charge -= neuron->priv->threshold;
		axon_fire (neuron->axon, neuron->priv->threshold);
	}
}
