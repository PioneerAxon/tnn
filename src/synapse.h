#ifndef __SYNAPSE_H__
#define __SYNAPSE_H__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

#include <neuron.h>
#include <axon.h>
#include <dendrite.h>

typedef struct _SynapsePrivate SynapsePrivate;
typedef struct _Axon Axon;
typedef struct _Dendrite Dendrite;
typedef struct _Neuron Neuron;

///
/// \brief Synapse structure
///
typedef struct _Synapse
{
	Dendrite* dendrite;		///< Dendrite instance
	Axon* axon;			///< Axon instance
	SynapsePrivate* priv;		///< Private components of Synapse
} Synapse;

///
/// \brief Synapse constructor
///
/// Creates a new instance of synapse.
///
/// \return Synapse instance
///
Synapse* synapse_new ();

///
/// \brief Synapse destructor
///
/// Destroy Synapse instance.
///
/// \param[in,out] synapse Synapse instance
///
void synapse_destroy (Synapse* synapse);

///
/// \brief Connect two Neurons
///
/// Connect source (from) Neuron 's Axon with destination Neuron's DendriteTree.
///
/// \param[in] synapse Synapse instance
/// \param[in] from Source Neuron
/// \param[in] to Destination Neuron
/// \param[in] weight Weight of connection
///
void synapse_connect_neurons (Synapse* synapse, Neuron* from, Neuron* to, float weight);

///
/// \brief Disconnect the Synapse
///
/// Disconnect already connected Neurons.
///
/// \param[in] synapse Synapse instance
///
void synapse_disconnect (Synapse* synapse);

///
/// \brief Connect Synapse and Axon
///
/// Connect Synapse and Axon instances.
///
/// \param[in] synapse Synapse instance
/// \param[in] axon Axon instance
/// \param[in] weight Weight of Synapse instance
///
void synapse_connect_axon (Synapse* synapse, Axon* axon, float weight);

///
/// \brief Set Axon of Synapze instance
///
/// Set Axon instance for Synapse instance.
///
/// \param[in] synapse Synapse instance
/// \param[in] axon Axon instance
///
void synapse_set_axon (Synapse* synapse, Axon* axon);

///
/// \brief Remove Axon from synapse
///
/// Remove Axon instance from Synapse instance.
///
/// \param[in] synapse Synapse instance
///
void synapse_remove_axon (Synapse* synapse);

///
/// \brief Connect Dendrite and Synapse
///
/// Connect Dendrite and Synapse instances.
///
/// \param[in] synapse Synapse instance
/// \param[in] dendrite Dendrite instance
/// \param[in] weight Weight of Synapse instance
///
void synapse_connect_dendrite (Synapse* synapse, Dendrite* dendrite, float weight);

///
/// \brief Set Dendrite of Synapse instance
///
/// \param[in] synapse Synapse instance
/// \param[in] dendrite Dendrite instance
///
void synapse_set_dendrite (Synapse* synapse, Dendrite* dendrite);

///
/// \brief Remove Dendrite from Synapse
///
/// Remove Dendrite instance from Synapse instance.
///
/// \param[in] synapse Synapse instance
///
void synapse_remove_dendrite (Synapse* synapse);

///
/// \brief Get Synapse weight
///
/// \param[in] synapse Synapse instance
///
/// \return Weight of Synapse instance (float).
///
float synapse_get_weight (Synapse* synapse);

///
/// \brief Increase weight of Synapse
///
/// Increase the bonding between Axon and Dendrite by increasing the weight.
///
/// \param[in] synapse Synapse instance
/// \param[in] percent Percent increase intended
///
void synapse_increase_weight (Synapse* synapse, float percent);

///
/// \brief Decrease weight of Synapse
///
/// Decrease the bonding between Axon and Dendrite by decreasing weight.
///
/// \param[in] synapse Synapse instance
/// \param[in] percent Percent decrease intended
///
void synapse_decrease_weight (Synapse* synapse, float percent);

///
/// \brief Transmit charge
///
/// Transmit charge from Synapse instance.
///
/// \param[in] synapse Synapse instance
/// \param[in] charge Charge
///
void synapse_transmit (Synapse* synapse, float charge);

#endif
