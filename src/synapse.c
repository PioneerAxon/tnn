#include <synapse.h>

///
/// \brief Private components of Synapse
///
typedef struct _SynapsePrivate
{
	float weight;		///< Weight of connection
} SynapsePrivate;

Synapse* synapse_new ()
{
	Synapse* new_synapse;
	new_synapse = (Synapse*) malloc (sizeof (Synapse));
	assert (new_synapse);
	new_synapse->axon = NULL;
	new_synapse->dendrite = NULL;
	new_synapse->priv = (SynapsePrivate*) malloc (sizeof (SynapsePrivate));
	assert (new_synapse->priv);
	return new_synapse;
}

void synapse_destroy (Synapse* synapse)
{
	if (!synapse)
		return;
	if (synapse->axon)
		axon_remove_synapse (synapse->axon, synapse);
	if (synapse->dendrite)
	{
		if (dendrite_get_dendritic_tree (synapse->dendrite))
			dendritic_tree_remove_synapse (dendrite_get_dendritic_tree (synapse->dendrite), synapse);
		else
			dendrite_destroy (synapse->dendrite);
	}
	if (synapse->priv)
		free (synapse->priv);
	free (synapse);
}

void synapse_connect_neurons (Synapse* synapse, Neuron* from, Neuron* to, float weight)
{
	synapse->priv->weight = weight;
	axon_add_synapse (from->axon, synapse);
	dendritic_tree_add_synapse (to->dendritic_tree, synapse);
}

void synapse_disconnect (Synapse* synapse)
{
	synapse->priv->weight = 0.0f;
	axon_remove_synapse (synapse->axon, synapse);
	dendritic_tree_remove_synapse (dendrite_get_dendritic_tree (synapse->dendrite), synapse);
}

void synapse_connect_axon (Synapse* synapse, Axon* axon, float weight)
{
	synapse->priv->weight = weight;
	axon_add_synapse (axon, synapse);
}

void synapse_set_axon (Synapse* synapse, Axon* axon)
{
	assert (synapse->axon == NULL);
	synapse->axon = axon;
}

void synapse_remove_axon (Synapse* synapse)
{
	synapse->axon = NULL;
}

void synapse_connect_dendrite (Synapse* synapse, Dendrite* dendrite, float weight)
{
	synapse->priv->weight = weight;
	if (dendrite_get_dendritic_tree (dendrite))
		dendritic_tree_add_synapse (dendrite_get_dendritic_tree (dendrite), synapse);
}

void synapse_set_dendrite (Synapse* synapse, Dendrite* dendrite)
{
	assert (synapse->dendrite == NULL);
	synapse->dendrite = dendrite;
}

void synapse_remove_dendrite (Synapse* synapse)
{
	synapse->dendrite = NULL;
}

float synapse_get_weight (Synapse* synapse)
{
	return synapse->priv->weight;
}

void synapse_increase_weight (Synapse* synapse, float percent)
{
	assert (percent >= 0.0f && percent <= 100.0f);
	synapse->priv->weight += (percent / 100.0f) * (1.0f - synapse->priv->weight);
}

void synapse_decrease_weight (Synapse* synapse, float percent)
{
	assert (percent >= 0.0f && percent <= 100.0f);
	synapse->priv->weight -= (percent / 100.0f) * (synapse->priv->weight);
}

void synapse_transmit (Synapse* synapse, float charge)
{
	dendrite_capture (synapse->dendrite, charge);
}
