#include <axon.h>

///
/// \brief Private components of Axon structure
///
typedef struct _AxonPrivate
{
	Neuron* neuron;		///< Parent Neuron instance
} AxonPrivate;

Axon* axon_new (Neuron* parent)
{
	Axon* new_axon;
	new_axon = (Axon*) malloc (sizeof (Axon));
	assert (new_axon);
	new_axon->synapse_count = 0;
	new_axon->synapses = NULL;
	new_axon->priv = (AxonPrivate*) malloc (sizeof (AxonPrivate));
	assert (new_axon->priv);
	new_axon->priv->neuron = parent;
	return new_axon;
}

void axon_destroy (Axon* axon)
{
	if (!axon)
		return;
	while (axon->synapse_count--)
	{
		synapse_remove_axon (axon->synapses [axon->synapse_count]);
		synapse_destroy (axon->synapses [axon->synapse_count]);
	}
	if (axon->synapses)
		free (axon->synapses);
	free (axon->priv);
	free (axon);
}

void axon_add_synapse (Axon* axon, Synapse* synapse)
{
	axon->synapses = (Synapse**) realloc (axon->synapses, sizeof (Synapse*) * (axon->synapse_count + 1));
	assert (axon->synapses);
	axon->synapses [axon->synapse_count] = synapse;
	axon->synapse_count++;
	synapse_set_axon (synapse, axon);
}

void axon_remove_synapse (Axon* axon, Synapse* synapse)
{
	int l;
	for (l = 0; l < axon->synapse_count; l++)
	{
		if (axon->synapses [l] == synapse)
		{
			axon->synapses [l] = axon->synapses [axon->synapse_count - 1];
			axon->synapses = (Synapse**) realloc (axon->synapses, sizeof (Synapse*) * (axon->synapse_count - 1));
			if (axon->synapse_count != 1)
				assert (axon->synapses);
			axon->synapse_count--;
			return;
		}
	}
}

void axon_fire (Axon* axon, float charge)
{
	float weights = 0.0f;
	int l;
	for (l = 0; l < axon->synapse_count; l++)
	{
		weights += synapse_get_weight (axon->synapses [l]);
	}
	for (l = 0; l < axon->synapse_count; l++)
	{
		synapse_transmit (axon->synapses [l], charge * synapse_get_weight (axon->synapses [l]) / weights);
	}
}
