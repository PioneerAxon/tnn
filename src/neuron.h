#ifndef __NEURON_H__
#define __NEURON_H__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

#include <neural_network.h>
#include <dendritic_tree.h>
#include <axon.h>

typedef struct _NeuronPrivate NeuronPrivate;
typedef struct _NeuralNetwork NeuralNetwork;
typedef struct _DendriticTree DendriticTree;
typedef struct _Axon Axon;

///
/// \brief Neuron structure
///
typedef struct _Neuron
{
	float charge;				///< Charge on Neuron instance
	DendriticTree* dendritic_tree;		///< DendriticTree of Neuron instance
	Axon* axon;				///< Axon of Neuron instance
	NeuronPrivate* priv;			///< Private components of Neuron instance
	int registered;				///< Boolean indicating registeration for a stimulation
} Neuron;

///
/// \brief Neuron constructor
///
/// Create new instance of Neuron for NeuralNetwork instance.
///
/// \param[in] neural_network NeuralNetwork instance
///
/// \return Neuron instance
///
Neuron* neuron_new (NeuralNetwork* neural_network);

///
/// \brief Neuron destructor
///
/// Destroy Neuron instance.
///
/// \param[in,out] neuron Neuron instance
///
void neuron_destroy (Neuron* neuron);

///
/// \brief Set threshold value
///
/// Set threshold value of Neuron instance.
///
/// \param[in] neuron Neuron instance
/// \param[in] threshold Threshold value
///
void neuron_set_threshold (Neuron* neuron, float threshold);

///
/// \brief Get threshold value
///
/// Get threshold value of Neuron instance.
///
/// \param[in] neuron Neuron instance
///
/// \return Current threshold value of Neuron instance
///
float neuron_get_threshold (Neuron* neuron);

///
/// \brief Stimulate Neuron
///
/// Queue Neuron for next calculation iteration of NeuralNetwork.
///
/// \param[in] neuron Neuron instance
///
void neuron_stimulate (Neuron* neuron);

///
/// \brief Process stimulated Neuron
///
/// Process stimulated Neuron, and fire, if required.
///
/// \param[in] neuron Neuron instance
///
void neuron_process_stimuli (Neuron* neuron);

#endif
