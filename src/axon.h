#ifndef __AXON_H__
#define __AXON_H__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

#include <neuron.h>
#include <synapse.h>

typedef struct _AxonPrivate AxonPrivate;
typedef struct _Neuron Neuron;
typedef struct _Synapse Synapse;

///
/// \brief Axon structure
///
typedef struct _Axon
{
	int synapse_count;	///< Connected synapse count
	Synapse** synapses;	///< Synapse array
	AxonPrivate* priv;	///< Private member of Axon structure
} Axon;

///
/// \brief Axon constructor
///
/// Create Axon instance for parent Neuron instance.
///
/// \param[in] parent Neuron instance
///
/// \return Axon instance
///
Axon* axon_new (Neuron* parent);

///
/// \brief Axon destructor
///
/// Destroy Axon instance
///
/// \param[in,out] axon Axon instance
///
void axon_destroy (Axon* axon);

///
/// \brief Add Synapse in Axon
///
/// Add Synapse instance in Axon instance.
///
/// \param[in] axon Axon instance
/// \param[in] synapse Synapse instance
///
void axon_add_synapse (Axon* axon, Synapse* synapse);

///
/// \brief Remove Synapse from Axon
///
/// Remove Synapse instance from Axon instance.
///
/// \param[in] axon Axon instance
/// \param[in] synapse Synapse instance
///
void axon_remove_synapse (Axon* axon, Synapse* synapse);

///
/// \brief Fire Axon
///
/// Fire charge from Axon.
///
/// \param[in] axon Axon instance
/// \param[in] charge Charge to be fired
///
void axon_fire (Axon* axon, float charge);

#endif
