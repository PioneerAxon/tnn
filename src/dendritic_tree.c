#include <dendritic_tree.h>

///
/// \brief Private components of DendriticTree
///
typedef struct _DendriticTreePrivate
{
	Neuron* neuron;		///< Parent Neuron instance
	float charge_delta;	///< Amount of charge change since last process
} DendriticTreePrivate;

DendriticTree* dendritic_tree_new (Neuron* parent)
{
	DendriticTree* new_dendritic_tree;
	new_dendritic_tree = (DendriticTree*) malloc (sizeof (DendriticTree));
	assert (new_dendritic_tree);
	new_dendritic_tree->dendrite_count = 0;
	new_dendritic_tree->dendrites = NULL;
	new_dendritic_tree->priv = (DendriticTreePrivate*) malloc (sizeof (DendriticTreePrivate));
	assert (new_dendritic_tree->priv);
	new_dendritic_tree->priv->neuron = parent;
	new_dendritic_tree->priv->charge_delta = 0.0f;
	return new_dendritic_tree;
}

void dendritic_tree_destroy (DendriticTree* dendritic_tree)
{
	if (!dendritic_tree)
		return;
	while (dendritic_tree->dendrite_count--)
	{
		dendrite_destroy (dendritic_tree->dendrites [dendritic_tree->dendrite_count]);
	}
	if (dendritic_tree->dendrites)
		free (dendritic_tree->dendrites);
	free (dendritic_tree->priv);
	free (dendritic_tree);
}

void dendritic_tree_add_synapse (DendriticTree* dendritic_tree, Synapse* synapse)
{
	Dendrite* dendrite;
	dendrite = dendrite_new (dendritic_tree);
	dendrite_set_synapse (dendrite, synapse);
	synapse_set_dendrite (synapse, dendrite);
	dendritic_tree_add_dendrite (dendritic_tree, dendrite);
}

void dendritic_tree_remove_synapse (DendriticTree* dendritic_tree, Synapse* synapse)
{
	Dendrite* dendrite;
	dendrite = synapse->dendrite;
	dendritic_tree_remove_dendrite (dendritic_tree, dendrite);
	synapse_remove_dendrite (synapse);
	dendrite_remove_synapse (dendrite);
	dendrite_destroy (dendrite);
}

void dendritic_tree_add_dendrite (DendriticTree* dendritic_tree, Dendrite* dendrite)
{
	dendritic_tree->dendrites = (Dendrite**) realloc (dendritic_tree->dendrites, sizeof (Dendrite*) * (dendritic_tree->dendrite_count + 1));
	assert (dendritic_tree->dendrites);
	dendritic_tree->dendrites [dendritic_tree->dendrite_count] = dendrite;
	dendritic_tree->dendrite_count++;
}

void dendritic_tree_remove_dendrite (DendriticTree* dendritic_tree, Dendrite* dendrite)
{
	int l;
	for (l = 0; l < dendritic_tree->dendrite_count; l++)
	{
		if (dendritic_tree->dendrites [l] == dendrite)
		{
			dendritic_tree->dendrites [l] = dendritic_tree->dendrites [dendritic_tree->dendrite_count - 1];
			dendritic_tree->dendrites = (Dendrite**) realloc (dendritic_tree->dendrites, sizeof (Dendrite*) * (dendritic_tree->dendrite_count - 1));
			if (dendritic_tree->dendrite_count != 1)
				assert (dendritic_tree->dendrites);
			dendritic_tree->dendrite_count --;
			return;
		}
	}
}

void dendritic_tree_notify_capture (DendriticTree* dendritic_tree, Dendrite* dendrite)
{
	dendritic_tree->priv->charge_delta += dendrite->charge;
	if (dendritic_tree->priv->neuron->registered == 0)
		if (dendritic_tree->priv->neuron->charge + dendritic_tree->priv->charge_delta >= neuron_get_threshold (dendritic_tree->priv->neuron))
			neuron_stimulate (dendritic_tree->priv->neuron);
	dendrite->charge = 0.0f;
}

float dendritic_tree_process_charge (DendriticTree* dendritic_tree)
{
	float ret = dendritic_tree->priv->charge_delta;
	dendritic_tree->priv->charge_delta = 0.0f;
	return ret;
}
