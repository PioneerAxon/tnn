#ifndef __DENDRITIC_TREE_H__
#define __DENDRITIC_TREE_H__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

#include <neuron.h>
#include <dendrite.h>
#include <synapse.h>

typedef struct _DendriticTreePrivate DendriticTreePrivate;
typedef struct _Neuron Neuron;
typedef struct _Dendrite Dendrite;
typedef struct _Synapse Synapse;

///
/// \brief Dendrite tree.
///
typedef struct _DendriticTree
{
	int dendrite_count;		///< Count of dendrite in structure
	Dendrite** dendrites;		///< Dendrite array
	DendriticTreePrivate* priv;	///< Private component of DendriticTree structure
} DendriticTree;

///
/// \brief DendriticTree constructor
///
/// Creates a new instance of DendriticTree.
///
/// \param[in] parent Neuron instance
///
/// \return DendriticTree instance
///
DendriticTree* dendritic_tree_new (Neuron* parent);

///
/// \brief DendriticTree destructor
///
/// Destroy an instance of DendriticTree.
///
/// \param [in,out] dendritic_tree DendriticTree instance
///
void dendritic_tree_destroy (DendriticTree* dendritic_tree);

///
/// \brief Add Synapse to DendriticTree
///
/// Add Synapse instance to DendriticTree instance.
///
/// \param[in] dendritic_tree DendriticTree instance
/// \param[in] synapse Synapse instance
///
void dendritic_tree_add_synapse (DendriticTree* dendritic_tree, Synapse* synapse);

///
/// \brief Remove Synapse from DendriticTree
///
/// Remove Synapse instance from DendriticTree instance.
///
/// \param[in] dendritic_tree DendriticTree instance
/// \param[in] synapse Synapse instance
///
void dendritic_tree_remove_synapse (DendriticTree* dendritic_tree, Synapse* synapse);

///
/// \brief Add Dendrite to DendriticTree
///
/// Add Dendrite instance to DendriticTree instance.
///
/// \param[in] dendritic_tree DendriticTree instance
/// \param[in] dendrite Dendrite instance
///
void dendritic_tree_add_dendrite (DendriticTree* dendritic_tree, Dendrite* dendrite);

///
/// \brief Remove Dendrite from DendriticTree
///
/// Remove Dendrite instance from DendriticTree instance.
///
/// \param[in] dendritic_tree DendriticTree instance
/// \param[in] dendrite Dendrite instance
///
void dendritic_tree_remove_dendrite (DendriticTree* dendritic_tree, Dendrite* dendrite);

///
/// \brief Notify capture of charge in Dendrite
///
/// Notify DendriticTree instance of charge capture event in Dendrite.
///
/// \param[in] dendritic_tree DendriticTree instance
/// \param[in] dendrite Dendrite instance
///
void dendritic_tree_notify_capture (DendriticTree* dendritic_tree, Dendrite* dendrite);

///
/// \brief Process charge changes in DendriticTree
///
/// Process changes in charge in DendriticTree instance.
///
/// \param[in] dendritic_tree DendriticTree instance
///
float dendritic_tree_process_charge (DendriticTree* dendritic_tree);

#endif
