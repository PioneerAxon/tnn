#include <dendrite.h>

///
/// \brief Private components of Dendrite.
///
typedef struct _DendritePrivate
{
	DendriticTree* dendritic_tree;		///< Parent DendriticTree instance
	Synapse* synapse;			///< Synapse instance
} DendritePrivate;

Dendrite* dendrite_new (DendriticTree* parent)
{
	Dendrite* new_dendrite;
	new_dendrite = (Dendrite*) malloc (sizeof (Dendrite));
	assert (new_dendrite);
	new_dendrite->charge = 0;
	new_dendrite->priv = (DendritePrivate*) malloc (sizeof (DendritePrivate));
	assert (new_dendrite->priv);
	new_dendrite->priv->dendritic_tree = parent;
	new_dendrite->priv->synapse = NULL;
	return new_dendrite;
}

void dendrite_destroy (Dendrite* dendrite)
{
	if (!dendrite)
		return;
	if (dendrite->priv->synapse)
	{
		synapse_remove_dendrite (dendrite->priv->synapse);
		synapse_destroy (dendrite->priv->synapse);
	}
	free (dendrite->priv);
	free (dendrite);
}

DendriticTree* dendrite_get_dendritic_tree (Dendrite* dendrite)
{
	return dendrite->priv->dendritic_tree;
}

void dendrite_set_synapse (Dendrite* dendrite, Synapse* synapse)
{
	dendrite->priv->synapse = synapse;
}

void dendrite_remove_synapse (Dendrite* dendrite)
{
	dendrite->priv->synapse = NULL;
}

void dendrite_capture (Dendrite* dendrite, float charge)
{
	dendrite->charge = charge;
	if (dendrite->priv->dendritic_tree)
		dendritic_tree_notify_capture (dendrite->priv->dendritic_tree, dendrite);
}
