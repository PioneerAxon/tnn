#include <neural_network.h>

///
/// \brief Private components of NeuralNetwork structure.
///
typedef struct _NeuralNetworkPrivate
{
	BinaryTree* current;
	BinaryTree* next;
} NeuralNetworkPrivate;

NeuralNetwork* neural_network_new ()
{
	NeuralNetwork* new_neural_network;
	new_neural_network = (NeuralNetwork*) malloc (sizeof (NeuralNetwork));
	assert (new_neural_network);
	new_neural_network->priv = (NeuralNetworkPrivate*) malloc (sizeof (NeuralNetworkPrivate));
	assert (new_neural_network->priv);
	new_neural_network->neuron_count = 0;
	new_neural_network->neurons = NULL;
	new_neural_network->priv->current = binary_tree_new ();
	new_neural_network->priv->next = binary_tree_new ();
	new_neural_network->input_dendrite_count = 0;
	new_neural_network->input_dendrites = NULL;
	new_neural_network->output_dendrite_count = 0;
	new_neural_network->output_dendrites = NULL;
	return new_neural_network;
}

void neural_network_destroy (NeuralNetwork* neural_network)
{
	if (!neural_network)
		return;
	while (neural_network->neuron_count--)
	{
		neuron_destroy (neural_network->neurons [neural_network->neuron_count]);
	}
	if (neural_network->neurons)
		free (neural_network->neurons);
	if (neural_network->input_dendrites)
		free (neural_network->input_dendrites);
	binary_tree_destroy (neural_network->priv->current);
	binary_tree_destroy (neural_network->priv->next);
	free (neural_network->priv);
	free (neural_network);
}

void neural_network_insert_neuron (NeuralNetwork* neural_network, Neuron* neuron)
{
	neural_network->neurons = (Neuron**) realloc (neural_network->neurons, sizeof (Neuron*) * (neural_network->neuron_count + 1));
	assert (neural_network->neurons);
	neural_network->neurons [neural_network->neuron_count] = neuron;
	neural_network->neuron_count++;
}

void neural_network_remove_neuron (NeuralNetwork* neural_network, Neuron* neuron)
{
	int l;
	for (l = 0; l < neural_network->neuron_count; l++)
	{
		if (neural_network->neurons [l] == neuron)
		{
			neural_network->neurons [l] = neural_network->neurons [neural_network->neuron_count - 1];
			neural_network->neurons = (Neuron**) realloc (neural_network->neurons, sizeof (Neuron*) * (neural_network->neuron_count - 1));
			neural_network->neuron_count--;
			return;
		}
	}
}

static void _neural_network_register_input_dendrite (NeuralNetwork* neural_network, Dendrite* dendrite)
{
	neural_network->input_dendrites = (Dendrite**) realloc (neural_network->input_dendrites, sizeof (Dendrite*) * (neural_network->input_dendrite_count + 1));
	assert (neural_network->input_dendrites);
	neural_network->input_dendrites [neural_network->input_dendrite_count] = dendrite;
	neural_network->input_dendrite_count++;
}

void neural_network_register_input_neuron (NeuralNetwork* neural_network, Neuron* neuron)
{
	Dendrite* dendrite;
	dendrite = dendrite_new (neuron->dendritic_tree);
	dendritic_tree_add_dendrite (neuron->dendritic_tree, dendrite);
	_neural_network_register_input_dendrite (neural_network, dendrite);
}

static void _neural_network_register_output_dendrite (NeuralNetwork* neural_network, Dendrite* dendrite)
{
	neural_network->output_dendrites = (Dendrite**) realloc (neural_network->output_dendrites, sizeof (Dendrite*) * (neural_network->output_dendrite_count + 1));
	assert (neural_network->output_dendrites);
	neural_network->output_dendrites [neural_network->output_dendrite_count] = dendrite;
	neural_network->output_dendrite_count++;
}

void neural_network_register_output_neuron (NeuralNetwork* neural_network, Neuron* neuron, float weight)
{
	Dendrite* dendrite;
	Synapse* synapse;
	dendrite = dendrite_new (NULL);
	synapse = synapse_new ();
	synapse_connect_axon (synapse, neuron->axon, weight);
	synapse_set_dendrite (synapse, dendrite);
	_neural_network_register_output_dendrite (neural_network, dendrite);
}

void neural_network_set_inputs (NeuralNetwork* neural_network, float* inputs)
{
	int l;
	assert (inputs);
	for (l = 0; l < neural_network->input_dendrite_count; l++)
	{
		dendrite_capture (neural_network->input_dendrites [l], inputs [l]);
	}
}

void neural_network_register_neuron_stimuli (NeuralNetwork* neural_network, Neuron* neuron)
{
	binary_tree_insert_data (neural_network->priv->next, neuron);
}

static void _neural_network_iterate_neuron_callback (void* data)
{
	neuron_process_stimuli ((Neuron*) data);
}

void neural_network_iterate (NeuralNetwork* neural_network)
{
	BinaryTree* swap;
	swap = neural_network->priv->current;
	neural_network->priv->current = neural_network->priv->next;
	neural_network->priv->next = swap;
	binary_tree_iterate_destroy (neural_network->priv->current, _neural_network_iterate_neuron_callback);
}

void neural_network_get_outputs (NeuralNetwork* neural_network, float* output)
{
	int l;
	assert (output);
	for (l = 0; l < neural_network->output_dendrite_count; l++)
	{
		output [l] = neural_network->output_dendrites [l]->charge;
		neural_network->output_dendrites [l]->charge = 0;
	}
}
